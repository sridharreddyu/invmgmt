/**
 * View Models used by Spring MVC REST controllers.
 */
package com.bits.invmgmt.web.rest.vm;
